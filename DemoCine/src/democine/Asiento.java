/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package democine;

/**
 *
 * @author AllanDeveloper
 */
public class Asiento {

    private char letra;
    private int numero;
    private int precio;
    private boolean disponible;

    public Asiento(char letra, int numero, int precio) {
        this.letra = letra;
        this.numero = numero;
        this.precio = precio;
        this.disponible = true;
    }

   /**
    * Obtiene el nombre completo del asiento
    * @return String letra + número
    */
    public String getNombre() {
        return String.valueOf(letra) + numero;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
